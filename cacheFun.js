
var cache={}
function cacheFunction(cb) {
    // Should return a funciton that invokes `cb`.
    // A cache (object) should be kept in closure scope.
    // The cache should keep track of all arguments have been used to invoke this function.
    // If the returned function is invoked with arguments that it has already seen
    // then it should return the cached result and not invoke `cb` again.
    // `cb` should only ever be invoked once for a given set of arguments.
    return((...names)=>{
       let flag=1
       for(let i=0;i<names.length;i++){
          if(!cache[names[i].toString()]){
            cache[names[i].toString()]=1
             flag=0
          }
       }
       
       if(flag){
         return(cache)
       }
       return cb
    })
  }

  module.exports=cacheFunction