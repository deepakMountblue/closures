var cacheFunction =require("./cacheFun")


const fun=(x)=>{
    if(typeof x==='function'){
        x()
    }
    else{
        console.log(x)
    }
}

let x=cacheFunction(()=>{
    console.log("callback")
})(1,2,3)


fun(x)

x=cacheFunction(()=>{
    console.log("callback")
})(1,2)

fun(x)

x=cacheFunction(()=>{
    console.log("callback")
})(1,2,4)

fun(x)

x=cacheFunction(()=>{
    console.log("callback")
})(1,4,2)

fun(x)