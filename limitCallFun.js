function limitFunctionCallCount(cb, n) {
    // Should return a function that invokes `cb`.
    // The returned function should only allow `cb` to be invoked `n` times.
    // Returning null is acceptable if cb can't be returned
    return(()=>{
       if(typeof cb !=='function'){
           console.log("first argument should be a callabck function ")
           return(null)
       }
        if(!n||n===0){
            console.log("Second argument should be a interger ")
            return(null)
        }
        if(!Number.isInteger(n)){
            console.log("argument should be a integer")
            return(null)
        }
        for(let i=0;i<n;i++){
            cb(i+1)
        }
  
    })
  }
  module.exports=limitFunctionCallCount